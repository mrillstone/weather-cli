import chalk from 'chalk';

const menus = {
    main: `
    ${chalk.greenBright('weather [command] <options>')}
    
        ${chalk.blackBright('now')}..........show current weather
        ${chalk.blackBright('forecast')}.....show weather forecast
        ${chalk.blackBright('config')}.......set API key, default city ID, default temp. units
        ${chalk.blackBright('version')}......show package version
        ${chalk.blackBright('help')}.........show help menu for command
    `,

    now: `//...
        `,
    forecast: `//...
        `,
    config: `//...
        `,

}

export async function help(args) {
    const subCmd = args._[0] == 'help'
        ? args._[1]
        : args._[0]
    console.log(menus[subCmd] || menus.main)
}