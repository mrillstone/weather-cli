import Conf from 'conf';
import Table from 'cli-table3';
import { configKey } from './configure';
import {
    validateApiKey,
    validateCityId,
    validateUnits,
    queryWeatherForecast
} from './utils';

export async function forecast(args) {
    const config = new Conf().get(configKey);
    const apiKey = args.apiKey || args.apikey || args.key || config.apiKey;
    if (!validateApiKey(apiKey)) {
        return;
    }

    const cityId = args.city || args.cityId || args.cityID || config.cityId;
    if (!validateCityId(cityId)) {
        return;
    }

    const units = args.units || args.unit || args.u || config.units;
    if (!validateUnits(units)) {
        return;
    }

    const { data } = await queryWeatherForecast(cityId, units, apiKey);

    const table = new Table({
        head: ['Date & Time', 'Weather', 'Temp'],
        colWidths: [23, 18, 7],
        wordWrap: true
    });
    console.log(table.toString());

}